# RuoYi脚手架

#### 介绍
相当于开发的地基  方便  稳定   安全
http://doc.ruoyi.vip/ruoyi-vue/ 【RuoYi-Vue介绍】

#### 软件架构
软件架构说明
基于SpringBoot、Spring Security、JWT、Vue&Elemrnt的前后端分离权限管理系统

redis性能高，因为redis主要是运行在内存中，也可以持久化保存到磁盘上，但是主要是在内存中，所以具有极高的读写性能，但是数据不能太大了，所以我们用redis保存用户的登陆认证信息等数据量不大，但是需要频繁读写的数据。

#### 安装教程

1.  https://gitee.com/y_project/RuoYi-Vue
基于SpringBoot、Spring Security、JWT、Vue&Elemrnt的前后端分离权限管理系统，同时提供了Vue3的版本
![输入图片说明](%E5%AE%89%E8%A3%85%E6%95%99%E7%A8%8B.jpg)
----------------------------------------------------------------------------------------
2.  cmder安装
参考下载链接 http://www.winwin7.com/soft/7148.html
cmder.rar
把Cmder.exe存放的目录添加到系统环境变量
添加 cmder 到右键菜单：在管理员权限的cmd下输入:  Cmder.exe /REGISTER ALL
任意目录，右键是否有cmd
---------------------------------------------------------------------------------------
3.  IEDA2020 安装
参考下载地址
https://www.jb51.net/softs/543007.html
ideaIU_2021.2.3_64bit_Portable.rar
解压到根目录
打开idea64.exe，没有提示试用就可，然后汉化，
集成汉化插件的步骤是 Ctrl+ALT+S 打开 Settings -> Plugins ，搜索Chinese安装，然后重启即可
---------------------------------------------------------------------------------------
4.   Apache Maven
参考下载地址
https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.zip
Maven 是一个项目管理和构建自动化工具，
解压后添加环境变量
PATH="c:\program files\apache-maven-3.x.y\bin"
Run "mvn --version" 能显示版本即可。
---------------------------------------------------------------------------------------
5.   JAVA安装
参考下载地址：https://dl.pconline.com.cn/download/1117483.html
jdk-8u181-windows-x64.exe
jdk8环境变量配置
新建用户变量：JAVA_HOME 、CLASSPATH 和系统Path
变量名：JAVA_HOME变量值 C:\Program Files\Java\jdk1.8.0_181
变量名：CLASSPATH变量值.%JAVA_HOME%/lib;%JAVA_HOME%/lib/tools.jar （注意最前面有一点）
变量名：Path变量值：%JAVA_HOME%\bin变量值%JAVA_HOME%\jre\bin（两个变量值）
测试环境变量配置是否成功。同时按住Win和R键，桌面左下角弹出‘运行’窗口，输入cmd，再回车；跳出DOS命令行窗口输入依次输入“JAVAC”、“java”、“java -version”
能正常运行即可
---------------------------------------------------------------------------------------
6.   redis 安装
参考下载地址：https://download.csdn.net/download/Koala_sea/85246542?utm_source=bbsseo
---------------------------------------------------------------------------------------
7.   MySQL
参考下载地址：https://www.jb51.net/softs/567496.html#downintro2
---------------------------------------------------------------------------------------
8.   navicat绿色
---------------------------------------------------------------------------------------
9.   谷歌浏览器
---------------------------------------------------------------------------------------

#### 使用说明

1.  安装ry-vue数据库
服务打开MySQL,再打开数据管理工具navicat，将两个数据库连接上；
![输入图片说明](%E6%9C%AC%E5%9C%B0%E6%95%B0%E6%8D%AE%E5%BA%93.jpg)
![输入图片说明](%E5%BB%BA%E7%AB%8B%E8%BF%9E%E6%8E%A5.jpg)
![输入图片说明](%E5%88%9B%E5%BB%BA%E6%96%B0%E6%95%B0%E6%8D%AE%E5%BA%93.jpg)
2.  将RuoYi目录中的sql目录的.sql文件导入新建数据库ry-vue中，右键【运行SQL文件】
3.  进入redis目录，右键Camder,输入redis-server,显示redis开启、访问端口号
4.  后端程序运行
用IDEA打开RuoYi项目，修改application-druid.yml，主库数据源的账号密码root
![输入图片说明](%E5%90%8E%E7%AB%AF%E5%A4%84%E7%90%86.jpg)
![输入图片说明](%E5%90%8E%E7%AB%AF%E5%90%AF%E5%8A%A8%E6%88%90%E5%8A%9F1.jpg)
![输入图片说明](%E5%90%8E%E7%AB%AF%E5%90%AF%E5%8A%A8%E6%88%90%E5%8A%9F2.jpg)
![输入图片说明](%E5%90%8E%E7%AB%AF%E5%90%AF%E5%8A%A8%E6%88%90%E5%8A%9F3.jpg)
5.  前端程序运行
进入下载目录ruoyi-ui,右键打开Cmder命令窗口，
输入npm install --registry=https://registry.npmmirror.com下载很多插件
成功后输入npm run dev执行
![输入图片说明](%E5%89%8D%E7%AB%AF%E6%93%8D%E4%BD%9C1.jpg)
![输入图片说明](%E6%89%A7%E8%A1%8C%E6%88%90%E5%8A%9F.jpg)
浏览器输入http://locahost，可看到登录界面，用户名是admin，密码是admin123
![输入图片说明](%E7%99%BB%E5%BD%95%E7%95%8C%E9%9D%A2.jpg)
![输入图片说明](%E7%BD%91%E9%A1%B51.jpg)
![输入图片说明](%E7%BD%91%E9%A1%B52.jpg)



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
